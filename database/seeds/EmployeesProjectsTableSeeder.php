<?php

use Illuminate\Database\Seeder;
use App\EmployeeProject;
use App\Employee;


class EmployeesProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeProject::truncate();
        EmployeeProject::create([
            'employee_id'=>1,
            'project_id'=>1,
            'role'=>Employee::ROLE_MANANGER
        ]);
        EmployeeProject::create([
            'employee_id'=>2,
            'project_id'=>1,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>3,
            'project_id'=>1,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>4,
            'project_id'=>1,
            'role'=>Employee::ROLE_DESIGNER
        ]);
        EmployeeProject::create([
            'employee_id'=>5,
            'project_id'=>2,
            'role'=>Employee::ROLE_DESIGNER
        ]);
        EmployeeProject::create([
            'employee_id'=>6,
            'project_id'=>2,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>7,
            'project_id'=>2,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>8,
            'project_id'=>2,
            'role'=>Employee::ROLE_MANANGER
        ]);
        EmployeeProject::create([
            'employee_id'=>9,
            'project_id'=>3,
            'role'=>Employee::ROLE_DESIGNER
        ]);
        EmployeeProject::create([
            'employee_id'=>10,
            'project_id'=>3,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>11,
            'project_id'=>3,
            'role'=>Employee::ROLE_DEVELOPER
        ]);
        EmployeeProject::create([
            'employee_id'=>12,
            'project_id'=>3,
            'role'=>Employee::ROLE_MANANGER
        ]);

    }
}
