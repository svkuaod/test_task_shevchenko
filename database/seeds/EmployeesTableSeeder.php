<?php

use Illuminate\Database\Seeder;
use App\Employee;
use Faker\Factory;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        Employee::truncate();

        for($i = 0; $i < 15; $i++){
            $city_id = 1;
            if($i%2 == 0){
                $city_id = 2;
            }
            Employee::create([
                'name'=>$faker->name(),
                'email'=>$faker->email,
                'city_id'=>$city_id
            ]);
        }

    }
}
