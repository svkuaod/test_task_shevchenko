<?php

use Illuminate\Database\Seeder;
use App\Project;
use Faker\Factory;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Project::truncate();
        Project::create([
            'title'=>$faker->company,
            'description'=>$faker->text(),
            'status'=>Project::STATUS_STARTED
        ]);
        Project::create([
            'title'=>$faker->company,
            'description'=>$faker->text(),
            'status'=>Project::STATUS_ALFA
        ]);
        Project::create([
            'title'=>$faker->company,
            'description'=>$faker->text(),
            'status'=>Project::STATUS_FINISHED
        ]);
    }
}
