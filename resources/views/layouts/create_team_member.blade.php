@extends('layouts.main')

@section('content')
<div class="container">
    <div class="col-md-12">
        <h1>List of free employees</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <a class="btn btn-success" href="{{route('team',['project'=>$project])}}">back</a>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Num</th>
                <th>name</th>
                <th>email</th>
                <th>city</th>
                <th>role</th>
                <th>action</th>
            </tr>
            <tbody>
            <div class="container">

                @foreach($employees as $key => $employee)
                    <form action="{{route('team_store',['project'=>$project])}}" method="POST">
                        <div class="form-group">
                            {{csrf_field()}}
                            <tr>
                                <th>{{$key+1}}</th>
                                <th>{{$employee->name}}</th>
                                <th>{{$employee->email}}</th>
                                <th>{{$employee->city->title}}</th>
                                <th>
                                    <select name="role" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{$role}}">{{$role}}</option>
                                        @endforeach
                                    </select>
                                </th>
                                <th>
                                    <input type="hidden" name="employee_id" value="{{$employee->id}}">
                                    <button type="submit" class="btn btn-success">add to project</button>
                                </th>
                            </tr>
                        </div>
                    </form>
                @endforeach
            </div>
            {{ $employees->links() }}
            </tbody>
            </thead>
        </table>
    </div>
</div>

@endsection
