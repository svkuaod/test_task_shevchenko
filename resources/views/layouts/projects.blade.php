@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="col-md-12">
            <h1>Projects list</h1>
            <a class="btn btn-success" href="{{URL('projects/create')}}">Add new project</a>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Num</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <tbody>
                <div class="container">

                    @foreach($projects as $key => $project)
                        <tr>
                            <th>{{$key+1}}</th>
                            <th><a href="{{route('team',['project'=>$project])}}">{{$project->title}}</a></th>
                            <th>{{$project->description}}</th>
                            <th>{{$project->status}}</th>
                            <th>
                                <a class="btn btn-primary" href="{{URL('projects/'. $project->id .'/edit')}}">edit</a>
                                <form action="{{URL('projects/' . $project->id)}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger">delete</button>
                                </form>

                            </th>
                        </tr>
                    @endforeach
                </div>
                </tbody>
                </thead>
            </table>
        </div>
    </div>
@endsection