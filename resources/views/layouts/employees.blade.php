@extends('layouts.main')
@section('content')
        <div class="container">
            <div class="col-md-12">
                <h1>Employees list</h1>
                <a class="btn btn-success" href="{{URL('employees/create')}}">Add new employee</a>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Num</th>
                        <th>name</th>
                        <th>email</th>
                        <th>city</th>
                        <th>action</th>
                    </tr>
                    <tbody>
                    <div class="container">

                    @foreach($employees as $key => $employee)
                        <tr>
                            <th>{{$key+1}}</th>
                            <th>{{$employee->name}}</th>
                            <th>{{$employee->email}}</th>
                            <th>{{$employee->city->title}}</th>
                            <th>
                                <a class="btn btn-primary" href="{{URL('employees/'  . $employee->id . '/edit')}}">edit</a>
                                <form action="{{URL('employees/' . $employee->id)}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger">delete</button>
                                </form>

                            </th>
                        </tr>
                    @endforeach
                    </div>

                    {{ $employees->links() }}
                    </tbody>
                    </thead>
                </table>
            </div>
        </div>
@endsection