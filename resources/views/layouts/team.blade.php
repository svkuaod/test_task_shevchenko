@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="col-md-12">
            <h1>Team list of {{$project->title}}</h1>
            <a class="btn btn-success" href=" {{route('team_create',['project'=>$project])}}">Add new member to project</a>

            <a class="btn btn-success" href="{{URL('projects/')}}">back</a>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Num</th>
                    <th>name</th>
                    <th>email</th>
                    <th>city</th>
                    <th>role</th>
                    <th>action</th>
                </tr>
                <tbody>
                <div class="container">
                    @foreach($team as $key => $member)
                        <tr>
                            <form action="{{route('team_update',['project'=>$member->project, 'employee'=>$member->employee])}}" method="POST">
                            <th>{{$key+1}}</th>
                            <th>{{$member->employee->name}}</th>
                            <th>{{$member->employee->email}}</th>
                            <th>{{$member->employee->city->title}}</th>
                            <th>
                                <select name="role" class="form-control">
                                    <option value="{{$member->role}}">{{$member->role}}</option>
                                    @foreach($roles as $role)
                                        @if($role != $member->role)
                                            <option value="{{$role}}">{{$role}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                    {{csrf_field()}}
                                    <button class="btn btn-primary">save</button>
                                </form>
                                <form action="{{route('team_delete', [ 'project'=>$member->project,'employee'=>$member->employee])}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button class="btn btn-danger">delete</button>
                                </form>

                            </th>
                        </tr>
                    @endforeach
                </div>
                {{ $team->links() }}
                </tbody>
                </thead>
            </table>
        </div>
    </div>
@endsection