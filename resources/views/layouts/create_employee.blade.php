@extends('layouts.main')

@section('content')
   <div class="container">
        <div class="col-md-12">
            <h1>Create new Employee form:</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{URL('employees')}}{{isset($employee) ? '/' . $employee->id : ''}}" method="POST">
                <div class="form-group">
                    {{csrf_field()}}
                    {{  isset($employee) ? method_field('PUT') : ''}}
                    <input name="name" type="text" placeholder="input name" class="form-control"
                    value="{{isset($employee) ? $employee->name : ''}}">
                    <input name="email" type="email" placeholder="input email" class="form-control"
                           value="{{isset($employee) ? $employee->email : ''}}">
                    <select name="city_id" class="form-control">
                        @if(!isset($employee))
                            <option value="" disabled selected>Select city</option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->title}}</option>
                                @endforeach
                            @else
                            <option value="{{$employee->city->id}}" >{{$employee->city->title}}</option>
                            @foreach($cities as $city)
                                @if($employee->city->title != $city->title)
                                <option value="{{$city->id}}">{{$city->title}}</option>
                                @endif
                            @endforeach
                        @endif

                    </select>
                    <button type="submit" class="btn btn-success">{{isset($employee) ? 'update' : 'create'}}</button>
                    <a class="btn btn-success" href="{{URL('employees/')}}">back</a>
                </div>
            </form>

        </div>
    </div>

@endsection
