@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="col-md-12">
            <h1>Create new Project form:</h1>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{URL('projects')}}{{isset($project) ? '/' . $project->id : ''}}" method="POST">
                <div class="form-group">
                    {{csrf_field()}}
                    {{  isset($project) ? method_field('PUT') : ''}}
                    <input name="title" type="text" placeholder="input title" class="form-control"
                           value="{{isset($project) ? $project->title : ''}}">
                    <input name="description" type="text" placeholder="input description" class="form-control"
                           value="{{isset($project) ? $project->description : ''}}">
                    <select name="status" class="form-control">
                        @if(!isset($project))
                            <option value="" disabled selected>Select status</option>
                            @foreach($statusArray as $status)
                                <option value="{{$status}}">{{$status}}</option>
                            @endforeach
                        @else
                            <option value="{{$project->status}}" >{{$project->status}}</option>
                            @foreach($statusArray as $status)
                                @if($project->status != $status)
                                    <option value="{{$status}}">{{$status}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                    <button type="submit" class="btn btn-success">{{isset($project) ? 'update' : 'create'}}</button>
                    <a class="btn btn-success" href="{{URL('projects/')}}">back</a>
                </div>
            </form>

        </div>
    </div>

@endsection
