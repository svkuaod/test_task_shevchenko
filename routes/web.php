<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.main');
});
Route::get('/home', function () {
    return view('layouts.main');
})->name('home');

Route::get('/employees', 'EmployeeController@index')->name('employees');
Route::get('/projects', 'ProjectController@index')->name('projects');
Route::get('/team/{project}', 'EmployeeProjectController@index')->name('team');
Route::get('/team/create/{project}', 'EmployeeProjectController@create')->name('team_create');
Route::post('/team/store/{project}', 'EmployeeProjectController@store')->name('team_store');
Route::post('/team/update/{project}/{employee}', 'EmployeeProjectController@update')->name('team_update');
Route::delete('/team/delete/{project}/{employee}', 'EmployeeProjectController@destroy')->name('team_delete');



Route::group(['prefix' => ''], function (){
    Route::resource('/employees','EmployeeController',['only' => ['create', 'edit', 'store', 'update', 'destroy']]);
    Route::resource('/projects','ProjectController',['only' => ['create', 'edit', 'store', 'update', 'destroy']]);
});



