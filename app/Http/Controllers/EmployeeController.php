<?php

namespace App\Http\Controllers;

use App\City;
use App\Employee;
use App\EmployeeProject;
use App\Http\Requests\StoreEmployeeRequest;

class EmployeeController extends Controller
{
    private $cities=[];

    public function __construct()
    {
        $this->cities = City::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::orderBy('id')
        ->paginate(10);
        return view('layouts.employees',['employees' => $employees]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.create_employee',['cities' => $this -> cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        $employee = [
            'name' => $request->name,
            'email' => $request->email,
            'city_id' => $request->city_id
        ];

        $result = Employee::insert($employee);
        return redirect('employees');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('layouts.create_employee',['cities' => $this->cities,'employee' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEmployeeRequest $request, Employee $employee)
    {
        $employee->update($request->all());
        return redirect('employees');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $result = $employee->delete();
        $deletedRows = EmployeeProject::where('employee_id', $employee->id)->delete();
        return redirect('employees');
    }
}
