<?php

namespace App\Http\Controllers;

use App\Employee;
use App\EmployeeProject;
use App\Project;
use Illuminate\Http\Request;

class EmployeeProjectController extends Controller
{
    private $roles = [];

    public function __construct()
    {
        $this->roles = Employee::getRoleArray();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
       $team = EmployeeProject::where('project_id',$project->id)
            ->orderBy('role')
            ->paginate(10);
        return view('layouts.team', ['team' => $team, 'project' => $project, 'roles' => $this->roles]);

        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Project $project)
    {
        $notFreeEmpl = EmployeeProject::pluck('employee_id')->all();
        $freeEmpl = Employee::whereNotIn('id', $notFreeEmpl)->select('*')->paginate(10);
        return view('layouts.create_team_member',['employees' => $freeEmpl,'roles' => $this->roles,'project' => $project]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $validatedData = $request->validate([
            'role' => 'required | string',
        ]);
        $member = [
            'employee_id' => $request->employee_id,
            'project_id' => $request->project->id,
            'role' => $request->role
            ];

        $result = EmployeeProject::insert($member);
        return redirect('team/'.$request->project->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Project $project,Employee $employee)
    {
        $validatedData = $request->validate([
            'role' => 'required | string',
        ]);
        $role = $request->role;
        $result = EmployeeProject::where('employee_id', $employee->id)
            ->where('project_id', $project->id)
            ->update(['role' => $role]);
        return redirect('team/' . $project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($project_id, $employee_id)
    {
        $deletedRows = EmployeeProject::where('employee_id', $employee_id)
            ->where('project_id', $project_id)
            ->delete();
        return redirect('team/' . $project_id);
    }
}
