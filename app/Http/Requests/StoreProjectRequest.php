<?php

namespace App\Http\Requests;

use App\Project;
use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->project) {
            $project = Project::find($this->project)->first();
        }
        else $project = null;
        return [
            'title' => 'required | min:3 | unique:projects,title' . ($project? ",$project->id" : ''),
            'description' => 'required | min:3 | max:250',
            'status' => 'required | string',
        ];
    }
}
