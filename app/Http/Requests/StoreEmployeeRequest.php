<?php

namespace App\Http\Requests;

use App\Employee;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->employee) {
            $employee = Employee::find($this->employee)->first();
        }
        else $employee = null;

        return [
            'name' => 'required | alpha | min:3 | max:50',
            'email' => 'email | required | unique:employees,email' . ($employee? ",$employee->id" : ''),
            'city_id' => 'required | int',
        ];
    }
}
