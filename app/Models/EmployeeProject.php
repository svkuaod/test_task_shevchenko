<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeProject extends Model
{
    protected $table = 'employees__projects';

    public function employee(){
        return $this->belongsTo('App\Employee');
    }

    public function project(){
        return $this->belongsTo('App\Project');
    }
}
