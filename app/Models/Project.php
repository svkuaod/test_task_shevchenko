<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const STATUS_STARTED = 'Started';
    const STATUS_ALFA = 'Alfa';
    const STATUS_BETA = 'Beta';
    const STATUS_RELEASE = 'Release';
    const STATUS_FINISHED = 'Finished';

    public static function getStatusArray(){
        $arr = array(self::STATUS_STARTED,self::STATUS_ALFA,self::STATUS_BETA,self::STATUS_RELEASE,self::STATUS_FINISHED);
        return $arr;
    }

    protected $table = 'projects';
    protected $fillable = ['title','description','status'];
}
