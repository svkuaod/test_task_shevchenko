<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Employee extends Model
{
    const ROLE_MANANGER = 'Mananger';
    const ROLE_DEVELOPER = 'Developer';
    const ROLE_DESIGNER = 'Designer';

    public static function getRoleArray(){
        $arr = array(self::ROLE_MANANGER,self::ROLE_DEVELOPER,self::ROLE_DESIGNER);
        return $arr;
    }
    protected $table = 'employees';
    protected $fillable = ['name','email','city_id'];

    public function city(){
        return $this->belongsTo('App\City');
    }

}
